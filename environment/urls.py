"""environment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from rest_framework import routers
from environment.dashboard import views
from environment.dashboard import upload
from django.contrib import admin

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'initial_mails', views.Initial_MailsViewSet)
from django.views.decorators.csrf import csrf_exempt

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('index', views.index),
    path('home/', views.home),
    path('dashboard/', views.dashboard),
    path('quality_dashboard/', views.quality_dashboard),
    path('action_module/', views.action_module),
    path('agent_performance_module/', views.agent_performance_module),
    path('login/', views.login),
    path(r'upload/', csrf_exempt(upload.uploadFile)),
    path('getTicketsGroupByOwnerGroup/', views.getTicketsGroupByOwnerGroup),
    path('getAllTickets/', views.getAllTickets),
    path('getAllTicketsByOwnerGroup/', views.getAllTicketsByOwnerGroup),
    path('getDataforStackedBar/', views.getDataforStackedBar),
    path('getAllInitialEmailsWithNoLogs/', views.getAllInitialEmailsWithNoLogs)

]
