import pandas as pd

PATH = '200 Tickets Scored data_Cleansed_Initial.xlsx'

raw_file = pd.read_excel(PATH)
print('Shape - before dropping: ', raw_file.shape)
drop_rows_for_cols = ['Notes_flown_to_Customer', 'i_ack', 'i_link_stat_chk', 'i_link stat_sim', 'i_logs',
                      'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr', 'i_na', 'i_gram']

raw_file.dropna(subset=drop_rows_for_cols, inplace=True)
raw_file.sort_values(by=['Number', 'Date_Of_Interact'], inplace=True)
print('Shape - after dropping: ', raw_file.shape)
raw_file.to_csv('intial_tickets.csv', index=False, encoding = 'utf-8')
print('Save file - intial_tickets.csv')
