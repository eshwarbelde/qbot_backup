import pymongo
from bson.json_util import dumps
import json
from django.http import JsonResponse
import datetime
import random

class MongoDbConnection:
    def __init__(self):
        client = pymongo.MongoClient(host='localhost', port=27017)
        self.db = client['db_email_classification']

    def insert_one_document(self, collection_name, data):
        collection = self.db[collection_name]
        inserted_record = collection.insert_one(data)
        return str(inserted_record.inserted_id)

    def getTicketsGroupByOwnerGroupAndRating(self, collection_name, circuit_type, severity, startdate, enddate):
    	print("Getting all tickets group by owner_group and rating")
    	collection = self.db[collection_name]
    	main_query = {}
    	group = {}
    	group_query = {}
    	match_query = {}
    	group['rating'] = "$rating"
    	group['owner_group'] = "$owner_group"
    	if(circuit_type != "All" and severity != "All"):
    		circuit_array = circuit_type.split(',')
    		severity_array = severity.split(',')
    		match_query = {"$match":{
    				"$and": [ 
				    			{"circuit_type" : {"$in":circuit_array}},
				    			{"severity" : {"$in":severity_array}}
	    			]
	    		}
	    	}
    	elif(circuit_type != "All" and severity == "All"):
    		array = circuit_type.split(',')
    		match_query["$match"] = {"circuit_type" : {"$in":array}}
    	elif(circuit_type == "All" and severity != "All"):
    		array = severity.split(',')
    		match_query["$match"] = {"severity" : {"$in":array}}

    	if(startdate != "" and enddate !=""):
    		import dateutil
    		start_date = dateutil.parser.parse(startdate)
    		end_date = dateutil.parser.parse(enddate)
    		match_query["$match"] = {"ticket_booking_time": { "$gte": start_date, "$lte" : end_date}}


    	group_query = {
    		"$group": {
    			"_id": group,
    			"count": { "$sum": 1 }
    		}
    	}
    	mydoc = ""
    	if(match_query != {}):
    		mydoc = collection.aggregate([
    			match_query,group_query
    		])
    	else:
    		mydoc = collection.aggregate([
    			group_query
    		])
    	return JsonResponse(dumps(mydoc), safe=False)

    def getAllTickets(self, collection_name, owner_group, circuit_type, severity, startdate, enddate):
    	print("Getting all tickets based on conditions")
    	collection = self.db[collection_name]
    	match_query = {}
    	where_query = []
    	
    	if(owner_group !='All'):
    		owner_group_array = owner_group.split(',')
    		where_query.append({"owner_group" : {"$in":owner_group_array}})
    	if(circuit_type != "All"):
    		circuit_array = circuit_type.split(',')
    		where_query.append({"circuit_type" : {"$in":circuit_array}})
    	if(severity != "All"):
    		severity_array = severity.split(',')
    		where_query.append({"severity" : {"$in":severity_array}})
    	if(startdate != "" and enddate !=""):
    		import dateutil
    		start_date = dateutil.parser.parse(startdate)
    		end_date = dateutil.parser.parse(enddate)
    		where_query.append({"ticket_booking_time": { "$gte": start_date, "$lte" : end_date}})


    	if(len(where_query) > 0):
    		match_query = {"$match":{
				"$and": where_query
				}
    		}
    	if(match_query != {}):
    		mydoc = collection.aggregate([
    			match_query
    		])
    	else:
    		mydoc = collection.find()
    	
    	# ratings_array = ["Perfect", "Good", "Needs Improvement"]
    	# final_object = []
    	# for ticket in mydoc:
    	# 	random_number = random.randint(0,2)
    	# 	ticket['rating'] = ratings_array[random_number]
    	# 	final_object.append(ticket)
    	return JsonResponse(dumps(mydoc), safe=False)

    def getAllTicketsByOwnerGroup(self, collection_name, owner_group, circuit_type, severity, startdate, enddate):
        print("Getting all intial emails group by rating and equal to seleted owner_group")
        collection = self.db[collection_name]
        where_query = []
        where_query.append({ "owner_group": { "$eq": owner_group }})
        if(circuit_type != "All"):
            circuit_array = circuit_type.split(',')
            where_query.append({"circuit_type" : {"$in":circuit_array}})
        if(severity != "All"):
            severity_array = severity.split(',')
            where_query.append({"severity" : {"$in":severity_array}})
        if(startdate != "" and enddate !=""):
            import dateutil
            start_date = dateutil.parser.parse(startdate)
            end_date = dateutil.parser.parse(enddate)
            where_query.append({"ticket_booking_time": { "$gte": start_date, "$lte" : end_date}})


        if(len(where_query) > 0):
            match_query = {"$match":{
                "$and": where_query
                }
            }
        if(match_query != {}):
            mydoc = collection.aggregate([
                match_query
            ])
        else:
            mydoc = collection.find({ "owner_group": { "$eq": owner_group } }, {"ticket_number":1, "_id":0})


        final_object = []
        for ticket in mydoc:
            final_object.append(ticket['ticket_number'])

        collection2 = self.db['initial_emails']
        initial_results = collection2.aggregate([
            {"$match":
                {
                    "ticket_number" : {"$in":final_object}
                }
			}
		])
        collection3 = self.db['subsequent_emails']

        subsequent_results = collection3.aggregate([
			{"$match":
				{
					"ticket_number" : {"$in":final_object}
				}
			}
    	])

        final_results = {
			"initial_emails" : initial_results,
			"subsequent_emails" : subsequent_results
		}

        return JsonResponse(dumps(final_results), safe=False)

    def getAllInitialEmailsWithNoLogs(self, collection_name):
        collection = self.db[collection_name]
        results = collection.find({"i_logs": 1})
        return JsonResponse(dumps(results), safe=False)
	

