class DbSchema:

        tickets_schema = {
                'ticket_number': {'type': 'string'},
                'ticket_booking_agent_name': {'type': 'string'},
                'ticket_booking_time': {'type': 'datetime'},
                'owner_group': {'type': 'string'},
                'service_id': {'type': 'string'},
                'severity': {'type': 'string'},
                'circuit_type': {'type': 'string'},
                'ticket_type': {'type': 'string'},
                'channel': {'type': 'string'},
                'rating': {'type': 'string'},
        }

        email_content_schema = {
                'ticket_number': {'type': 'string'},
                'email_number': {'type': 'integer'},
                'email_content': {'type': 'string'},
                'email_type': {'type': 'string'},
                'ticket_id': {'type': 'string'}
        }

        initial_emails_schema = {
                'ticket_number': {'type': 'string'},
                'initial_email_number': {'type': 'integer'},
                'i_sta_time': {'type': 'string'},
                'i_link_stat_chk': {'type': 'integer'},
                'i_link_stat_sim': {'type': 'integer'},
                'i_logs': {'type': 'integer'},
                'i_cust_dep': {'type': 'integer'},
                'i_poa': {'type': 'integer'},
                'i_time_1actual': {'type': 'string'},
                'i_time_2actual': {'type': 'string'},
                'i_time_1u': {'type': 'string'},
                'i_time_2u': {'type': 'string'},
                'i_resp_time': {'type': 'integer'},
                'i_time_update': {'type': 'string'},
                'i_time_benchmark': {'type': 'string'},
                'i_time': {'type': 'string'},
                'i_gram': {'type': 'string'},
                'i_spell': {'type': 'string'},
                'i_comp_rating': {'type': 'string'},
                'i_etr': {'type': 'integer'},
                'i_etr_time': {'type': 'string'},
                'i_svc_rstr': {'type': 'integer'},
                'ticket_id': {'type': 'string'},
        }

        subsequent_emails_schema = {
                
                'ticket_number':  {'type': 'string'},
                'subsequent_email_number': {'type': 'integer'},
                'si_sla_time': {'type': 'string'},
                'si_time_actual': {'type': 'string'},
                'si_etr': {'type': 'integer'},
                'si_etr_time': {'type': 'string'},
                'si_resp_time': {'type': 'integer'},
                'si_time_update': {'type': 'string'},
                'si_prog': {'type': 'integer'},
                'si_logs': {'type': 'integer'},
                'si_svc_rstr': {'type': 'integer'},
                'si_no_prob': {'type': 'integer'},
                'si_update': {'type': 'string'},
                'si_hold': {'type': 'string'},
                'si_time_benchmark': {'type': 'string'},
                's1_cd': {'type': 'integer'},
                'si_comp_rating': {'type': 'string'},
                'ticket_id': {'type': 'string'},
        }