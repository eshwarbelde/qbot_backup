from django.contrib.auth.models import User, Group
from .models import initial_mails, Ticket
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class TicketSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ticket
        fields = ['ticket_number', 'ticket_booking_agent_name', 'ticket_booking_time', 'owner_group', 'service_id',
            'severity', 'circuit_type', 'ticket_type', 'channel', 'rating']

class Initial_MailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = initial_mails
        fields = ['ticket_number', 'i_sla_time', 'i_link_stat_chk', 'i_link_stat_sim', 
        	'i_logs', 'i_cust_dep', 'i_poa', 'i_time_1actual', 'i_time_2actual', 'i_time_1u', 
        	'i_time_2u', 'i_resp_time', 'i_time_update', 'i_time_benchmak', 'i_time', 'i_gram',
        	'i_spell', 'i_comp_rating']
