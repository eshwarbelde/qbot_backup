#Email Classification


## Files/Folders:

- data = Input CSV/Excel files 
- models = Store all the models
- docs = Neccessary documented files.
- miscellaneous = Try out code files.
- output = Output files are stored.
- test_model.py = Train and test on the same data to get the accuracy and confusion matrix.
- training.py = Train the model of the given data.
- prediction.py = Predict the test data using trained model.
- preprocessing.py = It contains data cleansing and data normalization code.
- utils.py = Neccessary utilities are available.


## Commands:

##### For testing purpose and evalute the accuracy and confusion matrix:
- Train and test on the same data where 80% is used for training and 20% data are used for testing.

    Syntax -
    
    `python3 test_model.py "<path of the input file>"`
    
    Example - 
    
    `python3 test_model.py "data/200 Tickets Scored data_Cleansed_Initial.xlsx"`

#### For Production:

- **Step1:** Train the model first on the data:
    
    Syntax -
    
    `python3 training.py "<path of the input file>"`
    
    Example - 
    
    `python3 training.py "data/200 Tickets Scored data_Cleansed_Initial.xlsx"`

- **Step2:** Once the model is trained, predict the unseen or test data:

    Syntax -
    
    `python3 prediction.py`
    
    Example - 
    
    `python3 prediction.py`

