import pandas as pd
import sys
class data_filter:
    def __init__(self,file_path):
        self.filepath=file_path
        self.impact=['Total Loss of Service', 'Partial Loss']
        self.remove_products=['CAPNPLC','VNO','VNO Access','VNO Internet','VNO MPLS','VNO IPSec']
        self.current_state_remove=['Pending with customer','Suspended']
        
                
    def filtering(self):
        data=pd.read_excel(self.filepath)
        print('filtering initiated')
        data=data[~data['u_product'].isin(self.remove_products)]
        data=data[data['u_impact'].isin(self.impact)]
        data=data[data['Update Type.1']=='Customer']
        data=data[~data['u_current_state'].isin(self.current_state_remove)]
        file=str(data.sys_created_on.iloc[-1]).split(' ')[0]
        print("Save to excel initiated")
        path='environment/dashboard/output/ManOnNet_'+ file +'.xlsx'
        data.to_excel(path,index=False)
        print("File saved successfully")
        return data
    
if __name__=='__main__':
    data_filter(file_path="data/ManOnNet_2019-09-30.xlsx").filtering()
