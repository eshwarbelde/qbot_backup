import sys
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import pandas as pd

import utils
from preprocessing import Preprocessing
import matplotlib.pyplot as plt # Helps in plotting any graph


class TestModel(object):
    def __init__(self):
        self.file_path = sys.argv[1]
        self.validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_time_stamp',
                                 'i_link_stat_chk',
                                 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        self.predict_variables = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']  # Target variables.
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}

    @staticmethod
    def train_model(X, y):
        # Intialize classifier
        model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def predict_values(self, X_test, predict_variable):
        # Predict the target variable values of the test dataset on trained model.
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns):
        for v_col in self.validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def execute(self):
        df = self.preprocessing_obj.filter_data_by_email_method_2(self.file_path)
        self.are_columns_exist(df.columns)
        # Text preprocessing, stop word removal and stemming over the words of the text.
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(x))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.

        # Distribute the data into train and test datasets where 80% is used for training and 20% for testing.
        X = df_email_word_matrix
        for predict_variable in self.predict_variables:
            print(predict_variable)
            y = df[predict_variable]
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=25)
            model = self.train_model(X_train, y_train)
            self.models[predict_variable] = model
            y_pred = self.predict_values(X_test, predict_variable)

            if predict_variable == 'i_etr':
                for row_index, val in enumerate(y_pred):
                    if val == 1:
                        extracted_time = utils.extract_time(df.loc[row_index, 'Customer Update'])
                        print('i_etr_time: ', extracted_time)
            if predict_variable == 'i_resp_time':
                for row_index, val in enumerate(y_pred):
                    if val == 1:
                        extracted_time = utils.extract_time(df.loc[row_index, 'Customer Update'])
                        print('i_time_update: ', extracted_time)
            # Get accuracy score
            accuracy = round(accuracy_score(y_true=y_test, y_pred=y_pred), 2)
            print('Accuracy: ', accuracy)
            # Get confusion matrix
            binary = confusion_matrix(y_true=y_test, y_pred=y_pred)
            # plot confusion matrix
            plot_confusion_matrix(conf_mat=binary)
            plt.show()

if __name__=='__main__':
    TestModel().execute()