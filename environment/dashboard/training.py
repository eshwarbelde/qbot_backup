import sys
import pandas as pd
from utils import save_pickle_file
from preprocessing import Preprocessing
from sklearn.ensemble import RandomForestClassifier


class Training(object):
    def __init__(self):
        """
        Initialize the variables.
        """
        self.file_path = sys.argv[1]
        self.validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_time_stamp',
                                 'i_link_stat_chk',
                                 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        self.predict_variables = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']  # Target variables.
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}
        self.model_path = 'models/email_classifier_model_v2.pkl'
        self.dtm_model_path = 'models/document_term_matrix_model_v2.pkl'

    @staticmethod
    def train_model(X, y):
        """
        Train the classification model.
        :param X: Independent variables/features
        :param y: dependent variable/feature
        :return: Returns the model object after fitting on the training data.
        """
        model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def are_columns_exist(self, df_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in self.validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))


    def execute(self):
        """
        List of operations over the training data and build the model and store in the models directory.
        :return:
        """
        df = self.preprocessing_obj.filter_data_by_email_method_2(self.file_path)
        self.are_columns_exist(df.columns)
        # Text preprocessing, stop word removal and stemming over the words of the text.
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(x))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)
        save_pickle_file(self.dtm_model_path, count_vectorizer)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.

        for predict_variable in self.predict_variables:
            model = self.train_model(df_email_word_matrix, df[predict_variable])
            self.models[predict_variable] = model
        # Save all the models
        save_pickle_file(self.model_path, self.models)

if __name__=='__main__':
    Training().execute()