import json
import numpy as np

class ComplianceScore:
    def __init__(self):
        self.owner_groups = json.load(open('environment/dashboard/config/owner_group.json'))
        print(self.owner_groups)

    def get_records_based_on_sla_window(self, df_of_same_ticket_num):
        def _get_sla_time(row_x):
            if row_x.lower() in self.owner_groups:
                return self.owner_groups[row_x.lower()]['initial']
            return 0

        df_of_same_ticket_num = df_of_same_ticket_num.sort_values(by=['sys_created_on'])
        time_diff = df_of_same_ticket_num['sys_created_on'] - df_of_same_ticket_num['u_ticket_created_time']
        time_diff_in_min = round(time_diff/np.timedelta64(1,'m'), 2)
        df_sla_time = df_of_same_ticket_num['u_current_assignment_group'].apply(_get_sla_time)
        df_of_same_ticket_num['in_sla_window'] = time_diff_in_min <= df_sla_time
        df_within_sla_window = df_of_same_ticket_num[df_of_same_ticket_num['in_sla_window'] == True]
        df_outside_sla_window = df_of_same_ticket_num[df_of_same_ticket_num['in_sla_window'] == False]
        return df_within_sla_window, df_outside_sla_window

    @staticmethod
    def check_scenarios(data, columns):
        # columns.remove('i_link stat_sim')
        # columns.append('i_link_stat_sim')
        values = [data[x] for x in columns]
        if sum(values) >= 4:
            return 1, sum(values) # For Perfect
        elif sum(values) == 3:
            return 2, sum(values) # For Good
        else:
            return 3, sum(values) # For Need Improvement





if __name__=='__main__':
    ComplianceScore()