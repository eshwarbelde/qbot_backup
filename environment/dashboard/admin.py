from django.contrib import admin

# Register your models here.
from .models import initial_mails, Ticket

admin.site.register(initial_mails)
admin.site.register(Ticket)
