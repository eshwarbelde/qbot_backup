from django.template import RequestContext
from django.shortcuts import render
from .prediction import EmailClassification
from .filter_data import data_filter
import pandas as pd
from .predict_final import predictions
def uploadFile(request):
	print('File Upload Initiated')
	if request.method == 'POST':
		data_file = request.FILES['filename']
		
		data = data_filter(data_file).filtering()
		print(data.shape)
		EmailClassification(
	        file_path=data,
	        flag_save_in_csv=1, # To enable saving the data into csv file = 1 OR To disable saving the data into the csv file = None
	        flag_save_in_db=1 # To enable db storage = 1 OR To disable db storage = None
	    ).run()
		predictions(
			file_path='environment/dashboard/output/subsequent_conversation_ManOnNet_2019-09-30_new.csv',
			flag_save_in_db=1
		).run()

		return render(request,'home.html')
