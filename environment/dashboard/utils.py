import pickle
import re


def save_pickle_file(path, data):
    print('Saving the file: {}'.format(path))
    with open(path, 'wb') as fp:
        pickle.dump(data, fp)
        print('Saved in location: {}'.format(path))

def load_pickle_file(path):
    print('Loading the file: {}'.format(path))
    with open(path, 'rb') as fp:
        return pickle.load(fp)

def save_csv(path, df):
    print('Saving the CSV: {}'.format(path))
    df.to_csv(path, index=False)
    print('Output File: {}'.format(path))

def extract_time(text):
    regex = r"(\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
    matches = re.findall(regex, text, re.IGNORECASE)
    for match in matches:
        return ' '.join(match)