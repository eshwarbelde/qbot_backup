import copy
import pandas as pd # Operation over CSV or excel
import random
from environment.dashboard.compliance_score import ComplianceScore
from environment.dashboard.dbo.connection import MongoDbConnection
from environment.dashboard.preprocessing import Preprocessing # Custom class for preprocessing
from environment.dashboard.utils import load_pickle_file, save_csv, extract_time
from cerberus.validator import Validator
from environment.dashboard.dbo.db_schema import DbSchema


class EmailClassification:
    def __init__(self, file_path=None, flag_save_in_csv=None, flag_save_in_db=None):
        """
        Initialize neccessary variables
        """
        self.file_path = file_path
        self.flag_save_in_csv = flag_save_in_csv
        self.flag_save_in_db = flag_save_in_db
        self.mongodb_connection = MongoDbConnection()
        self.preserve_columns_order = []
        self.validate_columns = ['Customer Update', 'u_number', 'sys_created_on']
        self.predict_variables = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']  # Target variables.
        self.drop_columns_for_df = ['serial_no', 'customer_update_cleaned']
        self.drop_columns_for_initial = ['serial_no', 'customer_update_cleaned', 'email_type', 'compliance_sum_score']
        self.drop_columns_for_subsequent = ['serial_no', 'customer_update_cleaned', 'email_type',  'i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa',
                                            'i_svc_rstr', 'i_etr', 'i_resp_time',  'compliance_score', 'compliance_sum_score']
        self.drop_columns_for_discard = ['serial_no', 'customer_update_cleaned', 'email_type', 'compliance_sum_score']
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.initial_df = pd.DataFrame(columns=['i_time_update','i_etr_time'])
        self.subsequent_df = pd.DataFrame()
        self.discard_df = pd.DataFrame()
        self.models = {}
        self.predict_variables_values = {}
        self.model_path = 'environment/dashboard/models/email_classifier_model_v2.pkl'
        self.dtm_model_path = 'environment/dashboard/models/document_term_matrix_model_v2.pkl'
        self.output_csv_path = 'environment/dashboard/output/ManOnNet_2019-09-30_results_new.csv'
        self.initial_conversation_csv_path = 'environment/dashboard/output/initial_conversation_ManOnNet_2019-09-30_new.csv'
        self.subsequent_conversation_csv_path = 'environment/dashboard/output/subsequent_conversation_ManOnNet_2019-09-30_new.csv'
        self.discard_conversation_csv_path = 'environment/dashboard/output/discard_conversation_ManOnNet_2019-09-30_new.csv'
        self.schema_validator = Validator()
        print('Initialization has been done.')

    def predict_values(self, X_test, predict_variable):
        """
        Predict the target variable values of the test dataset on the trained model.
        """
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in self.validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def run(self):
        """
        List of operations over the test data and predict values on the loaded trained model.
        :return:
        """
        ratings_array = ["Perfect", "Needs Improvement"]
        df = self.preprocessing_obj.filter_drop_rows_by_emails(self.file_path)
        add_new_columns = self.predict_variables + ['compliance_score', 'compliance_sum_score', 'email_type',
                                                    'customer_update_cleaned']
        self.preserve_columns_order = list(df.columns) + add_new_columns
        df.fillna('', inplace=True)
        df = pd.concat([df, pd.DataFrame(columns=add_new_columns)], axis=1)
        self.are_columns_exist(df.columns)
        print('Validation on the columns has been done.')
        # Text preprocessing, stop word removal and stemming over the words of the text.
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(x))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))
        print('Text cleansing on the email contents has been done.')
        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer = load_pickle_file(self.dtm_model_path)
        dtm = self.preprocessing_obj.transform_document_term_vector(count_vectorizer, emails)
        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names(), index=df.index
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.
        self.models = load_pickle_file(self.model_path)
        print('Predicting the values for the emails: ')
        grouped_by_tickets = df.groupby(by=['u_number'])
        compliance_score_obj = ComplianceScore()
        i, len_grp_tickets = 0, len(grouped_by_tickets)
        for ticket_num, grp_ticket_num in grouped_by_tickets:
            flag_sla_outside = False
            i += 1
            print(i/len_grp_tickets * 100, end='\r')
            grped_by_ticket_num_within_sla, grped_by_ticket_num_outside_sla = compliance_score_obj.get_records_based_on_sla_window(grp_ticket_num)
            if not grped_by_ticket_num_within_sla.empty:
                grped_by_tickets = copy.deepcopy(grped_by_ticket_num_within_sla)
            else:
                flag_sla_outside = True
                grped_by_tickets = copy.deepcopy(grped_by_ticket_num_outside_sla)
            compliance_sum_scores = []
            compliance_scores = []
            for row_index, row_of_grped in grped_by_tickets.iterrows():
                df_each_email_word_matrix = df_email_word_matrix.loc[[row_index]]
                initial_emails_record_data = {}
                for predict_variable in self.predict_variables:
                    y_pred = self.predict_values(df_each_email_word_matrix, predict_variable)
                    if predict_variable not in self.predict_variables_values:
                        self.predict_variables_values[predict_variable] = []
                    predicted_value = int(y_pred[0])
                    self.predict_variables_values[predict_variable].append(predicted_value)
                    initial_emails_record_data[predict_variable] = predicted_value
                    df.loc[row_index, predict_variable] = predicted_value
                    grped_by_tickets.loc[row_index, predict_variable] = predicted_value

                    if predict_variable == 'i_etr':
                        if predicted_value == 1:
                            extracted_time = extract_time(df.loc[row_index, 'Customer Update'])
                            df.loc[row_index, 'i_etr_time'] = str(extracted_time)
                            grped_by_tickets.loc[row_index, 'i_etr_time'] = str(extracted_time)
                        else:
                            grped_by_tickets.loc[row_index, 'i_etr_time'] = ''

                    if predict_variable == 'i_resp_time':
                        if predicted_value == 1:
                            extracted_time = extract_time(df.loc[row_index, 'Customer Update'])
                            df.loc[row_index, 'i_time_update'] = str(extracted_time)
                            grped_by_tickets.loc[row_index, 'i_time_update'] = str(extracted_time)
                        else:
                            grped_by_tickets.loc[row_index, 'i_time_update'] = ''

                compliance_score, compliance_sum_score = compliance_score_obj.check_scenarios(initial_emails_record_data, self.predict_variables[:])
                df.loc[row_index, 'compliance_score'] = compliance_score
                df.loc[row_index, 'compliance_sum_score'] = compliance_sum_score
                compliance_scores.append(compliance_score)
                compliance_sum_scores.append(compliance_sum_score)
            grped_by_tickets['compliance_score'] = compliance_scores
            grped_by_tickets['compliance_sum_score'] = compliance_sum_scores

            if compliance_sum_scores and flag_sla_outside == False:
                max_compliance_sum_score = max(compliance_sum_scores)
                subsequent_inc = 0
                discard_inc = 0
                flag_discard_email = True
                flag_initial_email = True
                email_number = 0
                for row_index, row_of_grped in grped_by_tickets.iterrows():
                    email_number += 1
                    # Validate and save tickets in db
                    random_number = random.randint(0,1)
                    tickets_record_data = {
                        'ticket_number': df['u_number'][row_index],
                        'ticket_booking_time': df['u_ticket_created_time'][row_index],
                        'owner_group': df['u_current_assignment_group'][row_index],
                        'service_id': str(df['u_service_identifier'][row_index]),
                        'severity': df['u_impact'][row_index],
                        'circuit_type': df['u_product'][row_index],
                        # 'channel': df['Channe'][row_index],
                        'rating' : ratings_array[random_number]
                    }
                    if not self.schema_validator.validate(tickets_record_data, DbSchema.tickets_schema):
                        raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                    last_inserted_ticket_id = self.mongodb_connection.insert_one_document('tickets', tickets_record_data) \
                        if self.flag_save_in_db else None
                    if flag_discard_email and row_of_grped['compliance_sum_score'] < max_compliance_sum_score:
                        discard_inc += 1
                        email_type = 'discard' + str(discard_inc)  # discard<index> for discard
                        df.loc[row_index, 'email_type'] = email_type
                        self.discard_df = self.discard_df.append(df.loc[row_index], ignore_index=True)
                        print(self.discard_df.head())
                    elif flag_initial_email and row_of_grped['compliance_sum_score'] == max_compliance_sum_score:
                        flag_discard_email = False
                        flag_initial_email = False
                        email_type = 'i' # i for initial
                        df.loc[row_index, 'email_type'] = email_type
                        self.initial_df = self.initial_df.append(df.loc[row_index], ignore_index=True)
                        # Validate and save initial emails in db
                        random_number = random.randint(0,1)
                        initial_emails_record_data = {
                            'ticket_id': last_inserted_ticket_id,
                            'ticket_number': row_of_grped['u_number'],
                            'initial_email_number': email_number,
                            'i_link_stat_chk': row_of_grped['i_link_stat_chk'],
                            'i_link_stat_sim': row_of_grped['i_link stat_sim'],
                            'i_logs': row_of_grped['i_logs'],
                            'i_cust_dep': row_of_grped['i_cust_dep'],
                            'i_poa': row_of_grped['i_poa'],
                            'i_svc_rstr': row_of_grped['i_svc_rstr'],
                            'i_etr': row_of_grped['i_etr'],
                            'i_etr_time': row_of_grped['i_etr_time'],
                            'i_resp_time': row_of_grped['i_resp_time'],
                            'i_time_update': row_of_grped['i_time_update'],
                            'i_comp_rating': ratings_array[random_number]
                        }
                        if not self.schema_validator.validate(initial_emails_record_data, DbSchema.initial_emails_schema):
                            raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                        self.mongodb_connection.insert_one_document('initial_emails', initial_emails_record_data) \
                            if self.flag_save_in_db else None
                    else:
                        subsequent_inc += 1
                        email_type = 's' + str(subsequent_inc)  # s<index> for subsequent
                        df.loc[row_index, 'email_type'] = email_type
                        self.subsequent_df = self.subsequent_df.append(df.loc[row_index], ignore_index=True)

                    # Validate and Save Email content in db
                    email_content_record_data = {
                        'ticket_number': df['u_number'][row_index],
                        'email_content': str(df['Customer Update'][row_index]),
                        'email_number': email_number,
                        'email_type': email_type,
                        'ticket_id': last_inserted_ticket_id
                    }
                    if not self.schema_validator.validate(email_content_record_data, DbSchema.email_content_schema):
                        raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                    self.mongodb_connection.insert_one_document('email_content', email_content_record_data) \
                        if self.flag_save_in_db else None
            else:
                mark_initial = True
                subsequent_inc = 0
                email_number = 0
                for row_index, row_of_grped in grped_by_tickets.iterrows():
                    email_number += 1
                    random_number = random.randint(0,1)
                    # Validate and save tickets in db
                    tickets_record_data = {
                        'ticket_number': df['u_number'][row_index],
                        'ticket_booking_time': df['u_ticket_created_time'][row_index],
                        'owner_group': df['u_current_assignment_group'][row_index],
                        'service_id': str(df['u_service_identifier'][row_index]),
                        'severity': df['u_impact'][row_index],
                        'circuit_type': df['u_product'][row_index],
                        # 'channel': df['Channe'][row_index],
                        'rating' : ratings_array[random_number]

                    }
                    if not self.schema_validator.validate(tickets_record_data, DbSchema.tickets_schema):
                        raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                    last_inserted_ticket_id = self.mongodb_connection.insert_one_document('tickets', tickets_record_data) \
                        if self.flag_save_in_db else None

                    if mark_initial:
                        mark_initial = False
                        email_type = 'i' # i for initial
                        df.loc[row_index, 'email_type'] = email_type
                        self.initial_df = self.initial_df.append(df.loc[row_index], ignore_index=True)
                        random_number = random.randint(0,1)
                        # Validate and save initial emails in db
                        initial_emails_record_data = {
                            'ticket_id': last_inserted_ticket_id,
                            'ticket_number': row_of_grped['u_number'],
                            'initial_email_number': email_number,
                            'i_link_stat_chk': row_of_grped['i_link_stat_chk'],
                            'i_link_stat_sim': row_of_grped['i_link stat_sim'],
                            'i_logs': row_of_grped['i_logs'],
                            'i_cust_dep': row_of_grped['i_cust_dep'],
                            'i_poa': row_of_grped['i_poa'],
                            'i_svc_rstr': row_of_grped['i_svc_rstr'],
                            'i_etr': row_of_grped['i_etr'],
                            'i_etr_time': row_of_grped['i_etr_time'],
                            'i_resp_time': row_of_grped['i_resp_time'],
                            'i_time_update': row_of_grped['i_time_update'],
                            'i_comp_rating': ratings_array[random_number]
                        }
                        if not self.schema_validator.validate(initial_emails_record_data, DbSchema.initial_emails_schema):
                            raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                        self.mongodb_connection.insert_one_document('initial_emails', initial_emails_record_data) \
                            if self.flag_save_in_db else None
                    else:
                        subsequent_inc += 1
                        email_type = 's' + str(subsequent_inc)  # s<index> for subsequent
                        df.loc[row_index, 'email_type'] = email_type
                        self.subsequent_df = self.subsequent_df.append(df.loc[row_index], ignore_index=True)

                    # Validate and Save Email content in db
                    email_content_record_data = {
                        'ticket_number': df['u_number'][row_index],
                        'email_content': str(df['Customer Update'][row_index]),
                        'email_number': email_number,
                        'email_type': email_type,
                        'ticket_id': last_inserted_ticket_id
                    }
                    if not self.schema_validator.validate(email_content_record_data, DbSchema.email_content_schema):
                        raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                    self.mongodb_connection.insert_one_document('email_content', email_content_record_data) \
                        if self.flag_save_in_db else None

        # For whole df(contents)
        df = df[self.preserve_columns_order]
        df.sort_values(by='serial_no', inplace=True)
        df.drop(columns=self.drop_columns_for_df, axis=1, inplace=True)
        save_csv(self.output_csv_path, df) if self.flag_save_in_csv else None

        # For initial conversation df
        self.initial_df = self.initial_df[self.preserve_columns_order]
        self.initial_df.sort_values(by='serial_no', inplace=True)
        self.initial_df.drop(columns=self.drop_columns_for_initial, axis=1, inplace=True)
        save_csv(self.initial_conversation_csv_path, self.initial_df) if self.flag_save_in_csv else None

        # For subsequent conversation df
        self.subsequent_df = self.subsequent_df[self.preserve_columns_order]
        self.subsequent_df.sort_values(by='serial_no', inplace=True)
        self.subsequent_df.drop(columns=self.drop_columns_for_subsequent, axis=1, inplace=True)
        save_csv(self.subsequent_conversation_csv_path, self.subsequent_df) if self.flag_save_in_csv else None

        # For discard conversation df
        if list(self.discard_df.columns):
            self.discard_df = self.discard_df[self.preserve_columns_order]
            self.discard_df.sort_values(by='serial_no', inplace=True)
            self.discard_df.drop(columns=self.drop_columns_for_discard, axis=1, inplace=True)
        save_csv(self.discard_conversation_csv_path, self.discard_df) if self.flag_save_in_csv else None

        print('Finish prediciting...')

if __name__=='__main__':
    EmailClassification(
        file_path='data/ManOnNet_2019-09-30.xlsx',
        flag_save_in_csv=1,
        flag_save_in_db=1
    ).run()