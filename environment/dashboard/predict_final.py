import pandas as pd
import random
from environment.dashboard.preprocessing import Preprocessing
from environment.dashboard.utils import load_pickle_file, extract_time
from environment.dashboard.dbo.connection import MongoDbConnection
from environment.dashboard.dbo.db_schema import DbSchema
from cerberus.validator import Validator


class predictions:
    def __init__(self, file_path=None,flag_save_in_db=None):
    
        self.file_path = file_path
        self.preprocessing_obj = Preprocessing()
        self.mongodb_connection = MongoDbConnection()
        self.schema_validator = Validator()
        self.flag_save_in_db = flag_save_in_db
        self.dtm_model_path = 'environment/dashboard/models/countvect.pkl'
        self.s1_cd_path='environment/dashboard/models/s1_cd.pkl'
        self.si_etr_path='environment/dashboard/models/si_etr.pkl'
        self.si_logs_path='environment/dashboard/models/si_logs.pkl'
        self.si_no_prob_path='environment/dashboard/models/si_no_prob.pkl'
        self.si_prog_path='environment/dashboard/models/si_prog.pkl'
        self.si_svc_rstr_path='environment/dashboard/models/si_svc_rstr.pkl'
        self.si_resp_time_path='environment/dashboard/models/si_resp_time.pkl'
        self.subsequent_prediction_csv_path = 'environment/dashboard/output/subsequent_prediction.csv'
        self.result = pd.DataFrame(columns=['si_etr_time','si_time_update'])
     
    
    def run(self):
        ratings_array = ["Perfect", "Needs Improvement"]
        df=pd.read_csv(self.file_path)
        self.result=df.copy()
        #df=df[['Customer Update']]
        df['cleaned_notes'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text_subseuqent(x))
        corps = self.preprocessing_obj.corpus(df['cleaned_notes'])
        count_vectorizer = load_pickle_file(self.dtm_model_path)
        dtm=self.preprocessing_obj.transform_document_term_vector(count_vectorizer,corps)
        
        self.result['si_prog']=load_pickle_file(self.si_prog_path).predict(dtm)
        self.result['si_svc_rstr']=load_pickle_file(self.si_svc_rstr_path).predict(dtm)
        self.result['si_no_prob']=load_pickle_file(self.si_no_prob_path).predict(dtm)
        self.result['si_etr']=load_pickle_file(self.si_etr_path).predict(dtm)
        self.result['si_logs']=load_pickle_file(self.si_logs_path).predict(dtm)
        self.result['si_resp_time']=load_pickle_file(self.si_resp_time_path).predict(dtm)
        self.result['s1_cd']=load_pickle_file(self.s1_cd_path).predict(dtm)
        
        for index in range(len(self.result['Customer Update'])):
            time=extract_time(self.result['Customer Update'].loc[index])
            
            if time==None:
                self.result.loc[index,'si_etr_time']=''
                self.result.loc[index,'si_time_update']=''
                
            else:
                if self.result.loc[index,'si_etr']==1:
                    self.result.loc[index,'si_etr_time']=time
                    self.result.loc[index,'si_time_update']=''                    
                if self.result.loc[index,'si_resp_time']==1:
                    self.result.loc[index,'si_etr_time']=''
                    self.result.loc[index,'si_time_update']=time
                else:
                    self.result.loc[index,'si_etr_time']=''
                    self.result.loc[index,'si_time_update']=''
        print('predictions done!!!')
        
    
        if self.flag_save_in_db:
            print('writing files to database!!!')
        last_inserted_ticket_ids = []
        
        for row_index, row_of_grped in self.result.iterrows():
                  
                # Validate and save tickets in db
                
                # Validate and Save Email content in db
                random_number = random.randint(0,1)
                tickets_record_data = {
                    'ticket_number':row_of_grped['u_number'],
                    'si_prog': row_of_grped['si_prog'],
                    'si_svc_rstr': row_of_grped['si_svc_rstr'],
                    'si_no_prob': row_of_grped['si_no_prob'],
                    'si_etr': row_of_grped['si_etr'],
                    'si_etr_time':str(row_of_grped['si_etr_time']),
                    'si_logs': row_of_grped['si_logs'],
                    'si_resp_time': row_of_grped['si_resp_time'],
                    'si_time_update':str(row_of_grped['si_time_update']),
                    's1_cd': row_of_grped['s1_cd'],
                    'si_comp_rating': ratings_array[random_number]
                }
                if not self.schema_validator.validate(tickets_record_data, DbSchema.subsequent_emails_schema):
                    raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                last_inserted_ticket_id = self.mongodb_connection.insert_one_document('subsequent_emails', tickets_record_data)\
                    if self.flag_save_in_db else None
                    
                last_inserted_ticket_ids.append(last_inserted_ticket_id)
                
                email_content_record_data = {
                    'ticket_number': row_of_grped['u_number'],
                    'email_content': str(row_of_grped['Customer Update']),
                    'ticket_id': last_inserted_ticket_id
                }
                if not self.schema_validator.validate(email_content_record_data, DbSchema.email_content_schema):
                    raise KeyError('Invalid keys: {}'.format(self.schema_validator.errors))
                self.mongodb_connection.insert_one_document('email_content', email_content_record_data)\
                    if self.flag_save_in_db else None
                
        if self.flag_save_in_db:
            print('Data stored to database!!!')
                            
        print('Predictions saved to "{}"'.format(self.subsequent_prediction_csv_path))
        return self.result.to_csv(self.subsequent_prediction_csv_path,index=False)
        
if __name__=='__main__':
    predictions(
        file_path='data/subsequent_conversation_ManOnNet_2019-09-30_new.csv',
        flag_save_in_db=1
        
            ).run()