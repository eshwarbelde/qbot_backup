from django.contrib.auth.models import User, Group
from .models import initial_mails, Ticket
from rest_framework import viewsets
from rest_framework.response import Response
from environment.dashboard.serializers import UserSerializer, GroupSerializer, Initial_MailsSerializer, TicketSerializer
from django.shortcuts import render
#importing loading from django template  
from django.template import loader
# Create your views here.  
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
import pymongo
from bson.son import SON
import json
from environment.dashboard.dbo.connection import MongoDbConnection

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class Initial_MailsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows initialmails to be viewed or edited.
    """
    queryset = initial_mails.getAllInitialMails()
    serializer_class = Initial_MailsSerializer

class GetTicketsGroupByOwnerGroupViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer

def getTicketsGroupByOwnerGroup(request):
    mongodb_connection = MongoDbConnection()
    circuit_type = request.GET['circuit_type']
    severity = request.GET['severity']
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return mongodb_connection.getTicketsGroupByOwnerGroupAndRating('tickets', circuit_type, severity, startdate, enddate)

def getAllTickets(request):
    mongodb_connection = MongoDbConnection()
    owner_group = request.GET['owner_group']
    circuit_type = request.GET['circuit_type']
    severity = request.GET['severity']
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return mongodb_connection.getAllTickets('tickets', owner_group, circuit_type, severity, startdate, enddate)

def getAllTicketsByOwnerGroup(request):
    mongodb_connection = MongoDbConnection()
    owner_group = request.GET['owner_group']
    circuit_type = request.GET['circuit_type']
    severity = request.GET['severity']
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    print(owner_group)
    print(circuit_type)
    print(severity)
    print(startdate)
    print(enddate)
    return mongodb_connection.getAllTicketsByOwnerGroup('tickets', owner_group, circuit_type, severity, startdate, enddate)

def getDataforStackedBar(request):
    mongodb_connection = MongoDbConnection()
    tickets = request.GET['ticket_numbers']
    print(tickets)
    return mongodb_connection.getAllInitialEmailsByOwnerGroup('initial_emails', tickets)    

def getAllInitialEmailsWithNoLogs(request):
    mongodb_connection = MongoDbConnection()
    return mongodb_connection.getAllInitialEmailsWithNoLogs('initial_emails')

def home(request):
    template = loader.get_template('home.html') # getting our template
    return HttpResponse(template.render())  # rendering the template in HttpResponse  

def index(request):
    template = loader.get_template('index2.html')
    return HttpResponse(template.render())

def dashboard(request):
    template = loader.get_template('dashboard.html')
    return HttpResponse(template.render())

def quality_dashboard(request):
    template = loader.get_template('quality_dashboard.html')
    return HttpResponse(template.render())

def action_module(request):
    template = loader.get_template('action_module.html')
    return HttpResponse(template.render())
    
def agent_performance_module(request):
    template = loader.get_template('agent_performance_module.html')
    return HttpResponse(template.render())

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["qbot_database"]
        mycol = mydb["auth_user"]
        mydoc = mycol.find({'username': username,'password': password})
        if mydoc.count() > 0:
            return HttpResponseRedirect('/home/') 
        else:
            return render(request,'login.html')

    elif request.method == 'GET':
        return render(request,'login.html')