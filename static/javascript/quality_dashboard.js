// quality_dashboard.js
var ctx = "${pageContext.request.contextPath}",
  startDate = "",
  endDate = "",
  areachartStartDate = "",
  areachartEndDate = "",
  stackBarStartDate = "",
  stackBarEndDate = "",
  circuitTypeOldData = ['All'],
  severityOldData = ['All'],
  areaChartCircuitTypeOldData = ['All'],
  areaChartOwnerGroupOldData = ['All'],
  areaChartSeverityOldData = ['All'],
  stackBarCircuitTypeOldData = ['All'],
  stackBarSeverityOldData = ['All'];

$('#circuitType').val('All');
$('#severity').val('All');
$('#timeLine').val('All');
$('#areaChartTimeLine').val('All');
$('#areaChartCircuitType').val('All');
$('#areaChartOwnerGroup').val('All');
$('#areaChartSeverity').val('All');

$('#stackBarCircuitType').val('All');
$('#stackBarSeverity').val('All');

$(document).ready(function () {
  $('#circuitType').multiSelect();
  $('#severity').multiSelect();
  $('#areaChartCircuitType').multiSelect();
  $('#areaChartOwnerGroup').multiSelect();
  $('#areaChartSeverity').multiSelect();
  $('#stackBarCircuitType').multiSelect();
  $('#stackBarSeverity').multiSelect();

  getDataForPackedBubbleChart()
  getDataForAreaChart()
});

$('#circuitType').change(function () {
  window.circuitTypeOldData = dropDownOnChange('circuitType', window.circuitTypeOldData)
})
$('#severity').change(function () {
  window.severityOldData = dropDownOnChange('severity', window.severityOldData)
})
$('#areaChartCircuitType').change(function () {
  window.areaChartCircuitTypeOldData = dropDownOnChange('areaChartCircuitType', window.areaChartCircuitTypeOldData)
})
$('#areaChartOwnerGroup').change(function () {
  window.areaChartOwnerGroupOldData = dropDownOnChange('areaChartOwnerGroup', window.areaChartOwnerGroupOldData)
})
$('#areaChartSeverity').change(function () {
  window.areaChartSeverityOldData = dropDownOnChange('areaChartSeverity', window.areaChartSeverityOldData)
})
$('#stackBarCircuitType').change(function () {
  window.stackBarCircuitTypeOldData = dropDownOnChange('stackBarCircuitType', window.stackBarCircuitTypeOldData)
})
$('#stackBarSeverity').change(function () {
  window.stackBarSeverityOldData = dropDownOnChange('stackBarSeverity', window.stackBarSeverityOldData)
})

function dropDownOnChange(dropDownId, oldData) {
  var totalOptions = document.getElementById(dropDownId).length,
    data = $('#' + dropDownId).val();
  if (data[0] == 'All' && $('#' + dropDownId).val().length > 1) {
    if (oldData[0] == 'All') {
      document.getElementById(dropDownId + '_0').checked = false
      data.splice(0, 1);
      $('#' + dropDownId).val(data);
      return $('#' + dropDownId).val();
    } else {
      document.getElementById(dropDownId + '_0').checked = true
      $('#' + dropDownId).val(data.splice(0, 1))
      for (var i = 1; i < totalOptions; i++) {
        if ($('#' + dropDownId + '_' + i).is(':checked')) document.getElementById(dropDownId + '_' + i).checked = false
      }
      return $('#' + dropDownId).val();
    }
  } else return data;
}

$('#timeLine').change(function () {
  var timelineValue = $('#timeLine').val()
  var dates = timelineChange(timelineValue);

  window.startDate = dates['startDate'];
  window.endDate = dates['endDate'];

})

$('#areaChartTimeLine').change(function () {
  var timelineValue = $('#areaChartTimeLine').val()
  var dates = timelineChange(timelineValue);

  window.areachartStartDate = dates['startDate'];
  window.areachartEndDate = dates['endDate'];

})

$('#stackBarTimeLine').change(function () {
  var timelineValue = $('#stackBarTimeLine').val()
  var dates = timelineChange(timelineValue);

  window.stackBarStartDate = dates['startDate'];
  window.stackBarEndDate = dates['endDate'];

})

function timelineChange(timelineValue) {
  var dates = {}
  var currentDate = new Date();
  if (timelineValue == 'All') {
    dates['startDate'] = '';
    dates['endDate'] = '';
    return dates;
  } else if (timelineValue == 'Last 24 Hours') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 1)).toISOString();
    return dates;

  } else if (timelineValue == '48 Hours') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 2)).toISOString();
    return dates;

  } else if (timelineValue == '1 Week') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 7)).toISOString();
    return dates;

  } else if (timelineValue == '2 Weeks') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 14)).toISOString();
    return dates;

  } else if (timelineValue == '1 Month') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setMonth(currentDate.getMonth() - 1)).toISOString();
    return dates;

  } else if (timelineValue == 'Current Quarter') {

    var quarter = Math.floor((currentDate.getMonth() / 3));
    var strDate = new Date(currentDate.getFullYear(), quarter * 3, 1);
    var endDate = new Date(strDate.getFullYear(), strDate.getMonth() + 3, 0);
    dates['endDate'] = endDate.toISOString()
    dates['startDate'] = strDate.toISOString()
    return dates;
  }

}

function getDataForPackedBubbleChart() {

  var circuit_type = $('#circuitType').val();
  var severity = $('#severity').val();
  var start_date = window.startDate;
  var end_date = window.endDate;
  $.ajax({
    type: 'get',
    url: '/getTicketsGroupByOwnerGroup/?circuit_type=' + circuit_type +
      "&severity=" + severity + '&start_date=' + start_date + '&end_date=' + end_date,
    data: {
    },
    success: function (data) {
      var records = JSON.parse(data)
      console.log(records)
      var dataArray = [];
      for (var i = 0; i < records.length; i++) {
        var obj = records[i]._id
        var ownerGroup = dataArray.find(rec => rec.name == obj.owner_group)
        if (!ownerGroup) {
          dataArray.push({
            name: obj.owner_group,
            data: []
          })
          dataArray.find(rec => rec.name == obj.owner_group).data.push({
            name: obj.rating,
            value: records[i].count
          })
        } else {
          dataArray.find(rec => rec.name == obj.owner_group).data.push({
            name: obj.rating,
            value: records[i].count
          })
        }
      }
      console.log(dataArray)
      /*var BUArray = [], ASOCArray = [], ESSCArray = [], TxArray = []
      for(var i = 0; i<dataArray.length; i++) {
           if((dataArray[i].name).indexOf("ASOC") != -1)
              ASOCArray.push({name:dataArray[i].name, value: dataArray[i].data[0].value})
           if((dataArray[i].name).indexOf("ESSC") != -1)
              ESSCArray.push({name:dataArray[i].name, value: dataArray[i].data[0].value})
            if((dataArray[i].name).indexOf("Tx") != -1)
              TxArray.push({name:dataArray[i].name, value: dataArray[i].data[0].value})
      }    
      BUArray.push({name : "ASOC",data: ASOCArray, color: "#00cc33"});
      BUArray.push({name : "ESSC",data: ESSCArray, color: "#0066ff"});
      BUArray.push({name : "Tx",data: TxArray, color: "#ff6700"});*/

      var BUArray = [];
      BUArray.push({ name: "ASOC", data: [], records: [], color: "#00cc33" });
      BUArray.push({ name: "ESSC", data: [], records: [], color: "#0066ff" });
      BUArray.push({ name: "Tx", data: [], records: [], color: "#ff6700" });
      for (var z in BUArray) {
        for (var i = 0; i < dataArray.length; i++) {
          if (BUArray[z].name === (dataArray[i].name).substring(0, BUArray[z].name.length)) {
            BUArray[z].records.push(dataArray[i]);
            for (var j in dataArray[i].data) {
              var findRec = BUArray[z].data.find(rec => rec.name === dataArray[i].data[j].name)
              if (!findRec) {
                BUArray[z].data.push({
                  name: dataArray[i].data[j].name,
                  value: dataArray[i].data[j].value
                })
              } else BUArray[z].data.find(rec => rec.name === dataArray[i].data[j].name).value += dataArray[i].data[j].value
            }
          }
        }
      }
      console.log(BUArray);
      drawPackedBubbleChart('packedBubble', BUArray);
    },
    error: function (data) {
      console.log(data);
    }
  });
}
function fetchPackedBubbleChart(name, records) {
  $('#packedBubbleModalHead').html(name);
  $('#packedBubbleModal').modal("show");
  drawPackedBubbleChart('packedBubbleInModal', records);
}
function drawPackedBubbleChart(chartID, data) {
  Highcharts.chart(chartID, {
    chart: {
      type: 'packedbubble',
      height: '400'
    },
    title: {
      text: ''
    },
    tooltip: {
      useHTML: true,
      pointFormat: '<b>{point.name}:</b> {point.value}'
    },
    legend: false,
    credits: false,
    plotOptions: {
      series: {
        animation: false,
        cursor: 'pointer',
        point: {
          events: {
            click: function (e) {
              console.log(e);
              //fetchStackBar(e.target.point.series.name)
              if (e.target.point.series.userOptions.records) fetchPackedBubbleChart(e.target.point.series.name, e.target.point.series.userOptions.records)
              else fetchStackBar(e.target.point.series.name) //fetchStackBar(e.target.point.name) //JB change
            }
          }
        }
      },
      packedbubble: {
        //minSize: '20%',
        //maxSize: '20%',
        minSize: '20%',
        maxSize: '100%',
        layoutAlgorithm: {
          gravitationalConstant: 0.05,
          initialPositionRadius: 60,
          seriesInteraction: false,
          splitSeries: true,
          parentNodeLimit: true,
          dragBetweenSeries: false,
          parentNodeOptions: {
            bubblePadding: 20
          }
        },
        dataLabels: {
          enabled: true,
          format: '{point.shortName}',
          parentNodeFormat: '{point.series.name}'
        }
      }
    },
    series: data
  });
}

/**
 * Create the chart when all data is loaded
 * @returns {undefined}
 */
function getDataForAreaChart() {
  var owner_group = $('#areaChartOwnerGroup').val();
  var circuit_type = $('#areaChartCircuitType').val();
  var severity = $('#areaChartSeverity').val();
  var start_date = window.areachartStartDate;
  var end_date = window.areachartEndDate;
  $.ajax({
    type: 'get',
    url: '/getAllTickets/?owner_group=' + owner_group + '&circuit_type=' + circuit_type +
      "&severity=" + severity + '&start_date=' + start_date + '&end_date=' + end_date,
    data: {
    },
    success: function (data) {
      var records = JSON.parse(data), dataArray = [];
      records = records.sort((a, b) => {
        if (a.ticket_booking_time.$date < b.ticket_booking_time.$date) return -1;
        if (a.ticket_booking_time.$date > b.ticket_booking_time.$date) return 1;
        return 0;
      });
      for (var i in records) {
        var date = new Date(records[i].ticket_booking_time.$date)
        var formattedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ':' + date.getHours()
        var findRating = dataArray.find(rec => rec.name === records[i].rating);
        if (!findRating) {
          var ratingData = []
          ratingData.push([(date.valueOf() + 19800000), 1, formattedDate])
          dataArray.push({
            name: records[i].rating,
            data: ratingData
          })
        } else {
          var findDate = findRating.data.find(rec => rec[2] === formattedDate)
          if (findDate) {
            findDate[1]++
          } else {
            findRating.data.push([(date.valueOf() + 19800000), 1, formattedDate])
          }
        }
      }
      console.log(dataArray)
      createAreaChart(dataArray)
    },
    error: function (data) {
      console.log(data);
    }
  });
}
function createAreaChart(data) {
  Highcharts.stockChart('areaChart', {
    rangeSelector: {
      selected: 4
    },
    yAxis: {
      plotLines: [{
        value: 0,
        width: 2,
        color: 'silver'
      }],
      allowDecimals: false,
      min: 0
    },
    rangeSelector: {
      enabled: false
    },
    plotOptions: {
      series: {
        compare: 'percent',
        connectNulls: true,
        gapSize: 5,
        spacingTop: 10,
        pointInterval: 3600 * 1000,
        showInNavigator: true
      }
    },

    tooltip: {
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
      split: true
    },
    credits: false,
    series: data
  });
}

function fetchStackBar(ownerGroup) {
  var owner_group = ''
  if (ownerGroup) {
    owner_group = ownerGroup;
    // if(window.stackBarCircuitTypeOldData[0] !== 'All') window.stackBarCircuitTypeOldData.splice[0, 0, 'All'];
    // if(window.stackBarSeverityOldData[0] !== 'All') window.stackBarSeverityOldData.splice[0, 0, 'All'];
    // window.stackBarCircuitTypeOldData = window.stackBarCircuitTypeOldData.splice(0, 1);
    // window.stackBarSeverityOldData = window.stackBarSeverityOldData.splice(0, 1);
    window.stackBarCircuitTypeOldData = ['All'];
    window.stackBarSeverityOldData = ['All'];
    console.log(window.stackBarCircuitTypeOldData)
    console.log(window.stackBarSeverityOldData)

    document.getElementById('stackBarCircuitType_0').checked = true;
    document.getElementById('stackBarSeverity_0').checked = true;

    $('#stackBarCircuitType').val(window.stackBarCircuitTypeOldData);
    $('#stackBarSeverity').val(window.stackBarSeverityOldData);

    for (var i = 1; i < document.getElementById('stackBarCircuitType').length; i++) {
      console.log('#stackBarCircuitType_' + i)
      if ($('#stackBarCircuitType_' + i).is(':checked')) document.getElementById('stackBarCircuitType_' + i).checked = false
    }
    for (var i = 1; i < document.getElementById('stackBarSeverity').length; i++) {
      console.log('#stackBarSeverity_' + i)
      if ($('#stackBarSeverity_' + i).is(':checked')) document.getElementById('stackBarSeverity_' + i).checked = false
    }
  } else owner_group = $('#stackBarOwnerGroup').val();

  $('#stackBarOwnerGroup').val(owner_group);
  var circuit_type = $('#stackBarCircuitType').val();
  var severity = $('#stackBarSeverity').val();
  var start_date = window.stackBarStartDate;
  var end_date = window.stackBarEndDate;


  $.ajax({
    type: 'get',
    url: '/getAllTicketsByOwnerGroup/?owner_group=' + owner_group + '&circuit_type=' + circuit_type + '&severity=' + severity + '&start_date=' + start_date + '&end_date=' + end_date,
    data: {
    },
    success: function (data) {
      var records = JSON.parse(data)
      var seriesData = [], plotData = [], plotData1 = [], categories = ['Initial Emails', 'Subsequent Emails']; //['Initial emails', 'Subsequent emails', 'RFO'];
      var initial_records = records['initial_emails']
      var subsequent_records = records['subsequent_emails']
      $('#stackBarModalHead').html(owner_group);

      $('#stackBarModal').modal("show");
      for (var i in initial_records) {
        var findRating = plotData.find(rec => rec.name === initial_records[i].i_comp_rating);
        if (!findRating) {
          var rec = [];
          rec.push(initial_records[i]);
          plotData.push({ name: initial_records[i].i_comp_rating, data: [{ 'y': 1, 'records': rec }] });
        } else {
          findRating.data[0].y++;
          findRating.data[0].records.push(initial_records[i]);
        }
      }

      for (var i in subsequent_records) {
        var findRating = plotData1.find(rec => rec.name === subsequent_records[i].si_comp_rating);
        if (!findRating) {
          var rec = [];
          rec.push(subsequent_records[i]);
          plotData1.push({ name: subsequent_records[i].si_comp_rating, data: [{ 'y': 1, 'records': rec }] });
        } else {
          findRating.data[0].y++;
          findRating.data[0].records.push(subsequent_records[i]);
        }
      }
      console.log(seriesData)
      Highcharts.chart('stackBarChart', {
        chart: {
          type: 'column'
        },
        title: {
          text: ''
        },
        xAxis: {
          categories: categories
        },
        yAxis: {
          min: 0,
          title: {
            text: ''
          },
          stackLabels: {
            enabled: true,
            style: {
              fontWeight: 'bold',
              color: ( // theme
                Highcharts.defaultOptions.title.style &&
                Highcharts.defaultOptions.title.style.color
              ) || 'gray'
            }
          }
        },
        legend: {
          
          enabled: true,
          backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
          borderColor: '#CCC',
          borderWidth: 1,
          shadow: false
        },
        tooltip: {
          headerFormat: '<b>{point.x}</b><br/>',
          pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        credits: false,
        plotOptions: {
          column: {
            stacking: 'normal',
            dataLabels: {
              enabled: true
            },
            cursor: 'pointer',
            point: {
              events: {
                click: function (e) {
                  console.log(e);
                  console.log(e.target.point.series.name)
                  fetchStackBarInModal(e.target.point.category, e.target.point.series.name, e.target.point.records)
                }
              }
            }
          }
        },
        series: seriesData
      });
    },
    error: function (data) {
      console.log(data);
    }
  });
}
function fetchStackBarInModal(heading, rating, records) {
  var plotData = [],
    categories = ['Response Time', 'Link Check', 'Logs', 'PoA', 'CD', 'ETR', 'Service Restore'],
    categoriesKeys = ['i_resp_time', 'i_link_stat_chk', 'i_logs', 'i_poa', 'i_cust_dep', 'i_etr', 'i_svc_rstr'],
    seriesData = [];
  console.log(records);
  seriesData.push({
    name: '0',
    color: '#ED8F8F',
    data: []
  })
  seriesData.push({
    name: '1',
    color: '#84F3A9',
    data: []
  })
  for (var i in records) {
    _.map(records[i], function (val, key) {
      if (key.substring(0, 2) === 'i_') {
        if (!plotData.find(rec => rec.name === key)) plotData.push({ name: key, count: [0, 0] });
        if (val === 1) plotData.find(rec => rec.name === key).count[1]++
        else plotData.find(rec => rec.name === key).count[0]++
      }
    })
  }
  console.log(plotData)
  for (var i in seriesData) {
    for (var j in categoriesKeys) {
      var findRec = plotData.find(rec => rec.name === categoriesKeys[j])
      seriesData[i].data.push(findRec.count[i])
    }
  }
  console.log(seriesData)
  $('#stackBarModalInModalHead').html("Category: " + heading + ', Rating: ' + rating);
  $('#stackBarModalInModal').modal("show");
  Highcharts.chart('stackBarChartInModal', {
    chart: {
      type: 'column'
    },
    title: {
      text: ''
    },
    xAxis: {
      categories: categories
    },
    yAxis: {
      min: 0,
      title: {
        text: ''
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style &&
            Highcharts.defaultOptions.title.style.color
          ) || 'gray'
        }
      }
    },
    legend: {
      // align: 'right',
      // x: -30,
      // verticalAlign: 'top',
      // y: 25,
      // floating: true,
      enabled: true,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false
    },
    tooltip: {
      headerFormat: '<b>{point.x}</b><br/>',
      pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    credits: false,
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true
        }
      }
    },
    series: seriesData
  });
}