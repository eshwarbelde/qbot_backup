var wrongWordReplacements = [], checkTranslateType = 'Text', fileName= '', fileName= '', selectedFile = {}, selectedFrom = '', selectedTo = '', userText = '', grammarAPI = "Ginger";

$(document).ready(function(){
	if($("#resetPassword").val() === "true") {
		alert()
		location.href = window.ctx +'/resetPassword';
	}
	$.ajax({
		type: 'get',
		url: window.ctx +'/loadAllLanguages',
		data: {
		},
		success: function (resp) {
			window.languages = JSON.parse(resp).languages;
			const data = window.languages;
			console.log(data)
			var options = '';
			$.each( data, function( key, value ) {
				options = options + '<option value="' + value.code + '">' + value.name + '</option>';
			});

			$("#from-lang").append(options);
			$("#to-lang").append(options);
			$("#to-lang option[value=auto]").remove();
		},
		error: function (data) {
			console.log(data);
		}
	});
	 $(document).idleTimeout({
		 logout_url: window.ctx + '/LogoutServlet', // redirect to this url
	   //  idleTimeLimit: 5, // 15 seconds
	     inactivity: 180000,
	     //activityEvents: 'click keypress scroll wheel mousewheel', // separate each event with a space
	   //  dialogDisplayLimit: 30, // Time to display the warning dialog before logout (and optional callback) in seconds
	    // sessionKeepAliveTimer: false // Set to false to disable pings.
	    });
	/*$(document).idleTimeout({
	      redirectUrl:  '/logout', // redirect to this url. Set this value to YOUR site's logout page.
	      idleTimeLimit: 5,
	    });*/
	$("#dialog").dialog({
	    autoOpen : false, modal : true, show : "blind", hide : "blind", closeOnEscape: true, width: 400,
	    close: function(event, ui) { $('#wrap').show(); },
	    open: function(event, ui) 
	    { 
	        $('.ui-widget-overlay').bind('click', function()
	        { 
	            $("#dialog").dialog('close');
	        }); 
	    }
	  });
//	$("#dialog").dialog('widget').find(".ui-dialog-titlebar").hide();
	$("#feedbackDialog").dialog({
	    autoOpen : false, modal : true, show : "blind", hide : "blind", closeOnEscape: true, width: 600,
	    close: function(event, ui) { $('#wrap').show(); },
	    open: function(event, ui) 
	    { 
	        $('.ui-widget-overlay').bind('click', function()
	        { 
	            $("#feedbackDialog").dialog('close');
	        }); 
	    }
	  });
	var userRole = $("#userRole").val();
    if (userRole != 'USER') {
		$("#showDashboard").show();
		$("#showUsers").show();
    }
    else{
		$("#showDashboard").hide();
		$("#showUsers").hide();
    } 
    /*setTimeout(() => {
    	window.imageIndex++
    	if(window.imageIndex == 4) window.imageIndex = 1
		$('body').css("background", "url(assets/images/3.jpg)");
	}, 5000);*/
});
$('#translate').click(function() {
	clearTranslation();
	window.selectedFrom = $("#from-lang option:selected").val();
	window.selectedTo = $("#to-lang option:selected").val();
	window.userText = $("#userText").val();
	if (window.selectedTo === '' ) {
		var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
	    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
		$("#dialogMessage").text('Please select language to Translate');
		$("#dialog").dialog("open");
		closeModal();
	} else {
			if (window.fileName !== '') {
				getTranslatedDoc()
			} else if (window.userText === '') {
				var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
			    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
				$("#dialogMessage").text('Please enter text to translate');
				$("#dialog").dialog("open");
				closeModal();
			} else {
				getTranslatedText();
			}
	}   
 });

function getTranslatedDoc() {
	$("#lds-ring").show()
	$("#downloadFileImgDiv").hide();
	var fromLanguage =  window.selectedFrom;
	var toLanguage =  window.selectedTo;
	var type = 'Doc';
	var fileType = window.fileType
	if (fileType === 'application/pdf') {
		fileType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      }
	console.log(fromLanguage+"\t"+"\t"+toLanguage+"\t"+type+"\t"+fileType);
	var formData = new FormData();
	formData.append('fromLanguage',fromLanguage);
	formData.append('toLanguage',toLanguage);
	formData.append('type',type);
	formData.append('fileType',fileType);
	
	// Attach file
	formData.append('file', $('input[type=file]')[0].files[0]);
	$.ajax({
		url: 'InvokeURLServlet',
		type: 'POST',
		data: formData,
		contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
		processData: false,
		success: function(resp) {
			$("#lds-ring").hide();
			console.log(resp)
			if(resp && resp !== ''){
				resp = JSON.parse(resp);
				console.log(resp)
				if(resp.error === null) {
					$("#downloadFileImgDiv").show();
					var link = document.createElement('a')
					link.href = "/K7Text/InvokeURLServlet";
					document.body.appendChild(link)
					link.click()
				} else {
					console.log(resp)
					var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
				    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
					if(resp.message === 'ApiException'){
						$("#dialogMessage").text('Language pair not supported');
					} else if(resp.message === 'FileUploadException'){
						$("#dialogMessage").text('Unable to upload file. Please try again');
					} else {
						$("#dialogMessage").text('Something went wrong. Please try again');
					}
			        $("#dialog").dialog("open");
				}
			} else {
				console.log(resp)
				var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
			    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
				$("#dialogMessage").text('Something went wrong. Please try again');
		        $("#dialog").dialog("open");
			}
		},
		error: function(error) {
			$("#lds-ring").hide();
			console.log(error)
			var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
		    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
	        $("#dialogMessage").text('Something went wrong. Please try again');
	        $("#dialog").dialog("open");
		}
	});
}
function getTranslatedText() {
	$("#lds-ring").show();
	var data = window.userText
	var fromLangugae =  window.selectedFrom;
	var toLangugae =  window.selectedTo;
	var type = 'Text';
	var typeofGrammar = ""
	if(fromLangugae === 'en' && toLangugae === 'en'){
		/*$('#englishGrammarBtn').prop('checked', true).trigger("click");*/
		window.checkTranslateType = "Grammar";
		fetchTranslateType('Grammar');
		type = "Grammar";
		typeofGrammar = window.grammarAPI;
	}
	$.ajax({
	   url: $("#contextPath").val()+'/InvokeURLServlet',
	   type: 'POST',
	    data: {
	    "from" : fromLangugae,
	    "to" : toLangugae,
	    "data": data,
	    "type": type,
	    "grammarAPI" : typeofGrammar
	    },
	    success: function (resp) {
        	$("#lds-ring").hide();
        	resp = JSON.parse(resp)
	       if(fromLangugae == 'en' && toLangugae =='en') {
	    	   var textStr = '';
	    		   textStr += `<div>`;
	    		 if(window.grammarAPI === 'GrammarBot' && resp.matches.length !== 0) {
	    			var text = ''
	    				text += `<div style="width:100%;">`;
	    			var index = 0, strLength = data.length;
	    			setText(index, resp.matches[index].offset, resp.matches[index].offset + resp.matches[index].length)
	    			function setText(es, s, e) {
	    				if(es != s) {
	    					text+= `<span>` + data.substring(es, s) + `</span> `
	    				}
	    				 if(resp.matches[index].replacements.length > 0) {
	    					 text+= `<span class='wordtooltip'><span style="color: red; cursor: pointer;" id="wrongWord`+(index)+`" onmouseover="fetchWrongWord(`+(index)+`)">` + data.substring(s, e) + `</span><span class="wordtooltiptext"></span></span> `;
	    				 }
	    				 else {
	    					 text+= `<span>` + data.substring(s, e) + `</span> `;
	    				 }
	    				
	    				index++;
	    				if(resp.matches.length > index) setText(e, resp.matches[index].offset, resp.matches[index].offset + resp.matches[index].length)
	    				else text+= `<span>` + data.substring(e, strLength) + `</span> `
	    			}
	    			console.log(window.wrongWordReplacements)
	    			for(var i in resp.matches){
		    				var array = resp.matches[i].replacements
//		    				.length >= 4 ? resp.matches[i].replacements.slice(0, 4) : resp.matches[i].replacements;
		    				var replacements = '';
		    				replacements += `<div style="width:100%; text-align:center;">`;
		    				for(var j in array) {
		    					replacements += `<span id="`+i+`replacement`+j+`" class='row' style="margin-left: unset; margin-right:unset; margin:0px; padding:0px; font-weight: bold; font-size: 13px; color: blue; font-style: italic; cursor: pointer;" onclick="replacements(`+i+`,`+j+`)">` + array[j].value + ` </span>`;
		    				}
		    				replacements += `</div>`;
		    				window.wrongWordReplacements.push(replacements);
	    			}
	    			textStr += text;
	    		 } else if(window.grammarAPI === "Ginger" && resp.GingerTheDocumentResult.Corrections.length > 0){
	    			 var corrections = resp.GingerTheDocumentResult.Corrections
	    			 var text = ''
	    				text += `<div style="width:100%;">`;
	    			var index = 0, strLength = data.length;
	    			setText(index, corrections[index].From, corrections[index].To+1)
	    			function setText(es, s, e) {
	    				if(es != s) {
	    					text+= `<span>` + data.substring(es, s) + `</span> `
	    				}
	    				 if(corrections[index].Suggestions.length > 0) {
	    					 text+= `<span class='wordtooltip'><span style="color: red; cursor: pointer;" id="wrongWord`+(index)+`" onmouseover="fetchWrongWord(`+(index)+`)">` + data.substring(s, e) + `</span><span class="wordtooltiptext"></span></span> `;
	    				 }
	    				 else {
	    					 text+= `<span>` + data.substring(s, e) + `</span> `;
	    				 }
	    				
	    				index++;
	    				if(corrections.length > index) setText(e, corrections[index].From, corrections[index].To+1)
	    				else text+= `<span>` + data.substring(e, strLength) + `</span> `
	    			}
	    			console.log(window.wrongWordReplacements)
	    			for(var i in corrections){
		    				var array = corrections[i].Suggestions
//		    				.length >= 4 ? corrections[i].Suggestions.slice(0, 4) : corrections[i].Suggestions;
		    				var replacements = '';
		    				replacements += `<div style="width:100%; text-align:center;">`;
		    				for(var j in array) {
		    					replacements += `<span id="`+i+`replacement`+j+`" class='row' style="margin-left: unset; margin-right:unset; margin:0px; padding:0px; font-weight: bold; font-size: 13px; color: blue; font-style: italic; cursor: pointer;" onclick="replacements(`+i+`,`+j+`)">` + array[j].Text + ` </span>`;
		    				}
		    				replacements += `</div>`;
		    				window.wrongWordReplacements.push(replacements);
	    			}
	    			textStr += text;
	    		 } else {
	    			 textStr += `<span  style="width:100%;">` + data + `</span>`;
	    		 }
	    		 textStr += `</div>`;
	    		 $('#translatedText').html(textStr);
	        	} else if(resp.error === null){
	        		/*Systran success response*/
	        		
	        		var outputs = resp.outputs;
	    			var text = '';
	    			for (var i in outputs) {
	    				text += outputs[i].output;
	    			}
	        		$("#translatedText").html(text);
	        	} else if (resp) {
	            	console.log(resp)
	        		resp = JSON.parse(resp)
	        		if(resp.error){
	        			/*Azure error handling*/
		            	console.log(resp)
	        			var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
			    	    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
	                    if (resp.error.code === 400080){
	                    	$("#dialogMessage").html("Language pair is not supported");
	                    } else if (resp.error.code === 400050){
	                    	$("#dialogMessage").html("The input text is too long");
	                    } else if (resp.error.code === 400000){
	                    	$("#dialogMessage").html("Input text is not valid");
	                    } else {
	                    	$("#dialogMessage").html('Something went wrong. Please try again');
	                    }
	                    $("#dialog").dialog("open");
	        		} else {
	        			/*Azure success response*/
		            	console.log(resp)
	        			$("#translatedText").html(resp[0].translations[0].text);
	        		}
	            } else {
	        		/*Systran error handling*/
	            	
		        	console.log(resp);
		    		var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
		    	    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
		        	if(resp.message === 'ApiException'){
						$("#dialogMessage").html('Language pair not supported or <br/>Characters limit exceeded to 3000');
					} else {
						$("#dialogMessage").html('Something went wrong. Please try again');
					}
			        $("#dialog").dialog("open");
	        	}
	        },
	    error: function (error) {
	    	$("#lds-ring").hide();
	        console.log(error);
			var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
		    dialogTitle.html('<strong><i class="fa fa-exclamation-triangle"></i>&nbsp; Alert</strong>');
	        $("#dialogMessage").text('Something went wrong. Please try again');
	        $("#dialog").dialog("open");
	    }
});
	}

function showFeedbackDialog() {
	$('#commentEmpty').html("");
	$("#comment").val("");
	var dialogTitle = $('#feedbackDialog').closest('.ui-dialog').find('.ui-dialog-title');
    dialogTitle.html('<strong><i class="fa fa-comments"></i>&nbsp; Feedback</strong>');
	$("#feedbackDialog").dialog("open");
}

function feedbackGiven() {
	console.log($("#comment").val())
	if($("#comment").val() === '') {
		$('#commentEmpty').html("<font color=red>Please provide feedback</font>")
	} else {
		$.ajax({
		    url: $("#contextPath").val()+'/addFeedback',
		    type: 'POST',
	        data: {		
	        	"comments" : $("#comment").val()
	        },
	        success: function (resp) {
	        	console.log(resp);
	        	resp = JSON.parse(resp);
	        	if(resp.message="success"){
	            	$("#feedbackDialog").dialog("close");
	        		var dialogTitle = $('#dialog').closest('.ui-dialog').find('.ui-dialog-title');
	        	    dialogTitle.html('<strong><i class="fa fa-comments"></i>&nbsp; Feedback</strong>');
	            	$("#dialogMessage").text('Feedback added successfully');
					$("#dialog").dialog("open");
					closeModal();
	        	}

	        },		
	        error: function (error) {
	            console.log(error);
	        }		
	    });
	}		
}
$("#myfile").change(function(e) {
	console.log(e)
    var fileIcons= ['csv', 'xls', 'pdf', 'doc'];
    var files = e.target.files
    window.selectedFile = files[0]
    var fileExt = files[0].name.split('.').pop().substring(0, 3);
    var iconIndex = $.inArray(fileExt, fileIcons);
    var fileIcon = iconIndex == -1 ? 'file' : fileIcons[iconIndex];
    console.log(fileIcon)
    window.fileType = files[0].name.split('.').pop();
    window.fileName = files[0].name;
    
    /*$('#textTranslationBtn').prop('checked', true).trigger("click");*/
    fetchTranslateType('Text');
    /*$(".buttonGroup").attr("disabled", true);*/
    $("#userText").val('');
    $("#translatedText").html('');
    document.getElementById("userText").readOnly = true;
    document.getElementById("uploadFileImg").src = 'assets/images/'+fileIcon+'.png';
    $("#uploadFileImgDiv").show();
    document.getElementById("downloadFileImg").src = 'assets/images/'+(fileIcon == 'pdf' ? 'doc' : fileIcon)+'-icon.png';
    $("#copyTextDiv").hide();
    $("#pasteTextDiv").hide();
    
    if($("#from-lang option[value=auto]").length == 1) { $("#from-lang option[value=auto]").remove(); }
    $('#from-lang').val("en")
	$('#to-lang').prop('selectedIndex',0);
});  
function clearTranslation () {
    $("#translatedText").html("");
	$('.wordtooltiptext').html("");
	window.wrongWordReplacements = [];	
}
function cancelButton () {
	$("#lds-ring").hide();
	$("#downloadFileImgDiv").hide();
	$("#uploadFileImgDiv").hide();
	$("#userText").val('');
	$("#copyTextDiv").show();
	$("#pasteTextDiv").show();
	document.getElementById("userText").readOnly = false;

	window.selectedFile = {};
	window.fileType = '';
	window.fileName = '';
	$("#myfile").val("");
	
	/*$('#textTranslationBtn').prop('checked', true).trigger("click");*/
	fetchTranslateType('Text');
    /*$(".buttonGroup").attr("disabled", false);*/
	if($("#from-lang option[value=auto]").length == 0) { $("#from-lang").prepend('<option value="auto">Auto Detect</option>'); }
	$('#from-lang').prop('selectedIndex',0);
	$('#to-lang').prop('selectedIndex',0);
	
	clearTranslation()
}
function copyText() {
	  /*var justForCopy = ''
	  if (window.IsTextDivShown) {
	    justForCopy = document.getElementById('translatedText')
	    justForCopy.select()
	    document.execCommand('copy')
	  }*/
	
	  var textStr = document.getElementById('translatedText').innerText;
	  var el = document.createElement('textarea');
	   el.value = textStr;
	   el.setAttribute('readonly', '');
	   el.style = {position: 'absolute', left: '-9999px'};
	   document.body.appendChild(el);
	   el.select();
	   document.execCommand('copy');
	   document.body.removeChild(el);
	}
	function pasteText() {
		var userTextArea = document.getElementById('userText');
		userTextArea.value = '';
	    navigator.clipboard.readText()
	    .then((text)=>{
	    	userTextArea.value = text;
	    });
	}
function fetchWrongWord (id) {
	$('.wordtooltiptext').html("")
	console.log(window.wrongWordReplacements[id])
	$('.wordtooltiptext').append(window.wrongWordReplacements[id]);
}
function replacements (wordId, id) {
	var textSam = document.getElementById(wordId+"replacement"+id).innerHTML;
	$('#wrongWord'+wordId).text(textSam);
	document.getElementById('wrongWord'+wordId).style.color = 'green';
}
function fetchTranslateType(type){
	if(window.checkTranslateType !== type){
		window.checkTranslateType = type
		$("#userText").val('');
		clearTranslation();
	}
	if(type === 'Text') {
		$("#textTransalateTrue").show();
		$("#textTransalateFalse").hide();
		$("#grammarCheckTrue").hide();
		$("#grammarCheckFalse").show();
		$("#textTranslationBtn").css('opacity', 1);
		$("#englishGrammarBtn").css('opacity', 0.5);
		$('#from-lang').val("auto");
		$('#to-lang').val("");
		$('#from-lang').attr("disabled", false);
		$('#to-lang').attr("disabled", false);
	} else {
		$("#textTransalateTrue").hide();
		$("#textTransalateFalse").show();
		$("#grammarCheckTrue").show();
		$("#grammarCheckFalse").hide();
		$("#englishGrammarBtn").css('opacity', 1);
		$("#textTranslationBtn").css('opacity', 0.5);
		$('#from-lang').val("en");
		$('#to-lang').val("en");
		$('#from-lang').attr("disabled", true);
		$('#to-lang').attr("disabled", true);
	}
}
function closeModal() {
	var timer; // variable persisted here
    window.clearTimeout(timer);
    timer = window.setTimeout(function(){
    	console.log('--------Close-------')
    	$("#dialog").dialog("close");
    },5000); 
}
/*$(".buttonGroup").change(function (){
	clearTranslation()
	if(this.id === "textTranslationBtn"){
		$('#from-lang').val("auto");
		$('#to-lang').val("");
		$('#from-lang').attr("disabled", false);
		$('#to-lang').attr("disabled", false);
		
	} else {
		$('#from-lang').val("en");
		$('#to-lang').val("en");
		$('#from-lang').attr("disabled", true);
		$('#to-lang').attr("disabled", true);
	}
});*/