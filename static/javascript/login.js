var otp = '', isCorrectOtp = false, usersList = [];
$(document).ready(function() {
	$("#forgotPasswordDialog").dialog({
	    autoOpen : false, modal : true, show : "blind", hide : "blind", closeOnEscape: true,
        draggable: false,
		resizable: false
	  });
	getAllUsersData();
})
function getAllUsersData(){
	$.ajax({
		type: 'GET',
		url: 'UsersServlet',
		data: {
		},
		success: function (data) {
			window.usersList = JSON.parse(data);
		},
		error: function (data) {
			console.log(data);
		}
	});
}

$("#forgot").click(function(){
	var username = $("#username").val();
	if(username !== ""){
		$("#loadingImg").show();
		console.log(window.usersList)
		var isUserExist = window.usersList.find(userRec=> userRec.username === username)
		if(!isUserExist){
			$("#loadingImg").hide();
			$("#error").html("<font color=red>User doesn't exists. Please contact Admin</font>");
		} else {
			$("#loadingImg").show();
			$("#forgot").attr("disabled", true);
			$("#errorDiv").html("");
			$("#error").text("");
			getOTP()
		}
	} else {
		$("#error").html("<font color=red>Enter username (abc@example.com)</font>");
	}
});
function getOTP() {
	var username = $("#username").val();
	window.otp = '';
	$.ajax({
	    url: $("#contextPath").val()+'/forgotPassword',
	    type: 'POST',
	    data: {		
	    	"email": username
	    },		
	    success: function (resp) {
	    	window.otp = resp;
	    	$("#otpDiv").show();
			$("#passwordsDiv").hide();
	    	$("#loadingImg").hide();
	    	$("#forgot").attr("disabled", false);
	    	$("#emailSent").text(username);
	    	$("#userOtp").val("");
	    	$("#newPassword").val("");
	    	$("#reenterPassword").val("");
	    	$("#resetPasswardDiv").show();
	    	$("#loginDiv").hide();
	    	setTimeout(() => {
	    		window.otp = ' ';
			}, 180000);
//	    	$("#forgotPasswordDialog").dialog("open");
	   },		
	    error: function (error) {
	    	$("#loadingImg").hide();
	    	$("#forgot").attr("disabled", false);
	        console.log(error);
	    }	
	});
}
function checkOtp() {
	if($("#userOtp").val() == window.otp){
		window.isCorrectOtp = true;
		$("#otpDiv").hide();
		$("#passwordsDiv").show();
		$("#errorDiv").html("")
	} else {
		window.isCorrectOtp = false;
		$("#errorDiv").html("OTP is incorrect or is expired")
	}
}
function recoverPassword(){
	var username = $("#username").val();
	var newPassword = $("#newPassword").val();
	var reenterPassword = $("#reenterPassword").val();
	if(newPassword === '' || reenterPassword === ''){
		$("#errorDiv").html("Passwords should not empty")
	} else if(newPassword == reenterPassword){
		$.ajax({
		    url: $("#contextPath").val()+'/resetPassword',
		    type: 'POST',
		    data: {		
		    	"username": username,
		    	"password": newPassword
		    },		
		    success: function (resp) {
		    	resp = JSON.parse(resp)
		    	if(resp.status == "200"){
		    		$("#passwordsDiv").hide();
			    	$("#resetPasswardDiv").hide();
			    	$("#loginDiv").show();
//		    		$("#forgotPasswordDialog").dialog("close");
		    		$("#error").html("<font color=green> Password changed successfully</font>");
		    	} else{
		    		$("#errorDiv").html("Password change failed")
		    	}
		    },		
		    error: function (error) {
		        console.log(error);
		    }		
		});
	} else if(newPassword != reenterPassword){
		$("#errorDiv").text("Passwords doesnot match")
	}
}
function cancel() {
	$("#resetPasswardDiv").hide();
	$("#loginDiv").show();
}