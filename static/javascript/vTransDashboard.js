$(document).ready(function(){
				getDataforAreaChart();
				getDataforHalfPIEChart();
				getDataforSankeyChart();
				getAllFeedBacks();
			})
            var ctx = "${pageContext.request.contextPath}"
			var fromLanguages = [
					{textContent: "---Select Your Language---", value: ""},
	       			{textContent: "Arabic", value: "ar-EG"},
	       			{textContent: "German", value: "de-DE"},
	       			{textContent: "Spanish", value: "es-ES"},
	       			{textContent: "English", value: "en-US"},
	       			{textContent: "French", value: "fr-FR"},
	       			{textContent: "Italian", value: "it-IT"},
	       			{textContent: "Japanese", value: "ja-JP"},
	       			{textContent: "Russian", value: "ru-RU"},
	       			{textContent: "Chinese Simplified", value: "zh-CN"},
	       			{textContent: "Chinese Traditional", value: "zh-TW"},
	       			{textContent: "Portuguese (Brazil)", value: "pt-BR"},
	       			{textContent: "Dutch", value: "nl-NL"},
	       			{textContent: "Vietnamese", value: "vi-VN"},
	       			{textContent: "Malay", value: "ms-MY"}];
			function getDataforAreaChart(){
				 $.ajax({ type: 'get',
		          	   url: 'getAllFeedbacks', 
		          	   data: {
	         		   },
	         		   success: function (data) {
				          var data = JSON.parse(data)
				          console.log(data)
	          				var records = data.sort((a, b) => {
					            if (a.startTime < b.startTime) return -1
					            if (a.startTime > b.startTime) return 1
						            return 0
	          				})
				          var dataArray = []
	          			for (var i = 0; i < records.length; i++) {
				            var date = new Date(records[i].startTime)
				           	var formattedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ':' + date.getHours()
				            var findDate = dataArray.find((rec) => rec[2] === formattedDate)
				            if (findDate) {
				              findDate[1]++
				            } else {
				              dataArray.push([(date.valueOf() + 19800000), 1, formattedDate])
				            }
				          }
				          console.log(dataArray)
	         			  drawAreaChart(dataArray,'areaChart','All Translations');
	         		   },
	         		   error: function (data) {
	         			   console.log(data);
	         		   }
		      	   });
			}
			function getDataforHalfPIEChart(){
				 $.ajax({ type: 'get',
		          	   url: 'getAllFeedbacksGroupByRating', 
		          	   data: {
	        		   },
	        		   success: function (data) {
				          var records = JSON.parse(data);
				          console.log(records);
				          var result = [];
				            var total = 0;
				            for (var i = 0; i < records.length; i++) {
				              if (records[i]._id == null) {
				            	  records[i]._id = 0
				              }
				              result.push({
				                name: 'Rating- ' + records[i]._id,
				                y: records[i].count
				              })
				              total = total + records[i].count
				            }
				            result = result.sort((a, b) => { return a.name.split(' ').pop() - b.name.split(' ').pop() })
				            result.reverse()
				            var find = result.find(rec => rec.name === 'Rating- 0')
				            if (find) {
				              find.name = 'Unrated'
				            }
				          drawHalfPIEChart('halfPIEChart', result, total);
	        		   },
	        		   error: function (data) {
	        			   console.log(data);
	        		   }
		      	   });
			}
			function getDataforSankeyChart() {
				$.ajax({ type: 'get',
		          	   url: 'getAllFeedbacksGroupByLanguageAndRating', 
		          	   data: {
		     		   },
	     		   		success: function (data) {
	     		   			console.log(data)
				          var records = JSON.parse(data);
	     		   			console.log(records)
				            var result = getDataArrayForSankey(records);
				            var title = 'Language Pairs';
				           	drawSankeyChart('sankeyChart', title, result)
	     		   },
	     		   error: function (data) {
	     			   console.log(data);
	     		   }
		      	   });
			}
			function getDataArrayForSankey(data) {
			      var dataArray = []
			      var total = 0
			      for (var i = 0; i < data.length; i++) {
			        var formattedLanguages = data[i]._id.fromLanguage + '-' + data[i]._id.toLanguage
			        var findRecord = dataArray.find((rec) => rec.fromTo === formattedLanguages)
			        if (findRecord) {
			          console.log(findRecord)
			          findRecord.weight = findRecord.weight + data[i].count
			          if (data[i]._id.rating === 5) findRecord.fiveRating = data[i].count
			          else if (data[i]._id.rating === 4) findRecord.fourRating = data[i].count
			          else if (data[i]._id.rating === 3) findRecord.threeRating = data[i].count
			          else if (data[i]._id.rating === 2) findRecord.twoRating = data[i].count
			          else if (data[i]._id.rating === 1) findRecord.oneRating = data[i].count
			          else findRecord.noRating = findRecord.noRating + data[i].count
			        } else {
			          var fiveRating = 0
			          var fourRating = 0
			          var threeRating = 0
			          var twoRating = 0
			          var oneRating = 0
			          var noRating = 0
			          if (data[i]._id.rating === 5) {
			            fiveRating = data[i].count
			          } else if (data[i]._id.rating === 4) {
			            fourRating = data[i].count
			          } else if (data[i]._id.rating === 3) {
			            threeRating = data[i].count
			          } else if (data[i]._id.rating === 2) {
			            twoRating = data[i].count
			          } else if (data[i]._id.rating === 1) {
			            oneRating = data[i].count
			          } else {
			            noRating = data[i].count
			          }
			          dataArray.push({
			            from: findLanguage(data[i]._id.fromLanguage),
			            to: findLanguage(data[i]._id.toLanguage) + ' ',
			            weight: data[i].count,
			            fromTo: formattedLanguages,
			            fiveRating: fiveRating,
			            fourRating: fourRating,
			            threeRating: threeRating,
			            twoRating: twoRating,
			            oneRating: oneRating,
			            noRating: noRating
			          })
			          total = total + data[i].count
			        }
			      }
			      var result = {
			        'dataArray': dataArray,
			        'total': total
			      }
			      return result
		    }
			function drawAreaChart(dataArray, selectedTabName, title) {
			      Highcharts.StockChart(selectedTabName, {
			        chart: {
			        	spacingTop: 20,
			        	spacingBottom: 20,
			        	spacingLeft: 20,
			        	spacingRight: 40
			        },
			        credits: {
			          enabled: false
			        },
			        xAxis: {
			          type: 'datetime',
			          ordinal: false,
			          gapGridLineWidth: 0
			        },
			        yAxis: {
			          allowDecimals: false,
			          min: 0
			        },
			        rangeSelector: {
			          enabled: false
			        },
			        tooltip: {
			          split: false
			        },
			        series: [{
			          name: 'No. of Translations',
			          type: 'area',
			          data: dataArray,
			          dataLabels: {
			            enabled: true
			          },
			          gapSize: 5,
			          spacingTop: 10,
			          pointInterval: 3600 * 1000,
			          connectNulls: true,
			          fillColor: {
			            linearGradient: {
			              x1: 0,
			              y1: 0,
			              x2: 0,
			              y2: 1
			            },
			            stops: [
			              [0, Highcharts.getOptions().colors[0]],
			              [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
			            ]
			          },
			          threshold: null
			        }]
			      })
		    }
			function drawHalfPIEChart(containerName, result, total) {
			      Highcharts.chart(containerName, {
			        chart: {
			          plotBackgroundColor: null,
			          plotBorderWidth: 0,
			          plotShadow: false,
			          spacingTop: 20,
	        		  spacingBottom: 20,
		        	  spacingLeft: 20,
		        	  spacingRight: 40
			        },
			        title: {
			          text: 'Total: ' + total,
			          align: 'center',
			          verticalAlign: 'middle',
			          y: 40
			        },
			        credits: {
			          enabled: false
			        },
			        tooltip: {
			          headerFormat: '',
			          pointFormat: '<b>{point.name}</b>'
			        },
			        plotOptions: {
			          pie: {
			            dataLabels: {
			              enabled: true,
			              distance: -50,
			              format: '{point.y}',
			              style: {
			                fontSize: '15px',
			                textOutline: false
			              }
			            },
			            startAngle: -90,
			            endAngle: 90,
			            center: ['50%', '75%'],
			            size: '110%',
			            showInLegend: true,
			            pointInterval: 24 * 3600 * 1000
			          }
			        },
			        series: [{
			          type: 'pie',
			          innerSize: '50%',
			          data: result
			        }]
			      })
		    }
			function drawSankeyChart(containerName, title, result) {
			      Highcharts.chart(containerName, {
			        title: {
			          text: ''
			        },
			        // subtitle: {
			        //   text: '<b>Total: ' + result.total + '</b>'
			        // },
			        credits: {
			          enabled: false
			        },
			        tooltip: {
			          headerFormat: '',
			          useHTML: true,
			          pointFormat: '<b>{point.from} - {point.to} : {point.weight}</b>' +
			            '<br><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span> : {point.fiveRating}' +
			            '<br><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star unchecked"></span> : {point.fourRating}' +
			            '<br><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span> : {point.threeRating}' +
			            '<br><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span> : {point.twoRating}' +
			            '<br><span class="fa fa-star checked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span> : {point.oneRating}' +
			            '<br><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span><span class="fa fa-star unchecked"></span> : {point.noRating}'
			        },
			        series: [{
			          data: result.dataArray,
			          type: 'sankey'
			        }]
			      })
		    }
			function getAllFeedBacks(){
				$.ajax({ type: 'get',
		          	   url: 'getAllFeedbacks', 
		          	   data: {
		     		   },
	     		   		success: function (data) {
				          var records = JSON.parse(data);
	     		   			console.log(records)
	     		   			var finalData = []
	     		   			for(var i in records){
	     		   				if(records[i].comments != ""){
									finalData.push(records[i])
	     		   				}
	     		   			}
	     		   		console.log(finalData)
	     		   		$('#dataTable').html( '<table id="table" class="table display table-hover" style="background-color:white; font-size: 14px"></table>' );

	     				$('#table').dataTable( {
	     					"columnDefs":[{targets:[5], class:"wrapok"},
	     						{"targets":0, "type":"date"}],
	     					"order": [[ 0, "desc" ]],
     					 	"autoWidth": false,
	     					"lengthChange":false,
	     					"data": finalData,
	     					"columns": [
	     							    {"title":"StartTime","":"","render": function (data, type, full, meta) {
	     			   						return msToDate(full.startTime); }},
										{"title":"Duration","":"","render": function (data, type, full, meta) {
	     			   						return msToTime(full.duration); }},
		     					        {"title":"From","":"","render": function (data, type, full, meta) {
	     			   						return findLanguage(full.fromLanguage); }},
	     			   					{"title":"To","":"","render": function (data, type, full, meta) {
	     			   						return findLanguage(full.toLanguage); }},
		     					      {"title":"Rating","data":"rating"},
		     					     {"title":"Comments","data":"comments"}
	     					],
	     				} );		
	     				/* $("#table tr td").each(function() {
	     					        var cellText = $.trim($(this).text());
	     					        if (cellText.length == 0) {
	     					            $(this).parent().hide();
	     					        }
	     					    }); */

	     		   },
	     		   error: function (data) {
	     			   console.log(data);
	     		   }
		      	   });
			}
			function msToTime(duration) {
				  var milliseconds = parseInt((duration % 1000) / 100),
				    seconds = Math.floor((duration / 1000) % 60),
				    minutes = Math.floor((duration / (1000 * 60)) % 60),
				    hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

				  hours = (hours < 10) ? "0" + hours : hours;
				  minutes = (minutes < 10) ? "0" + minutes : minutes;
				  seconds = (seconds < 10) ? "0" + seconds : seconds;

				  return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
			}
			function msToDate(duration) {
				var today = new Date(duration)
				var timestamp =   (today.getDate() < 10 ? '0'+today.getDate() : today.getDate()) + '-' + ((today.getMonth()+1) < 10 ? '0'+(today.getMonth()+1) : (today.getMonth()+1)) + '-' +  today.getFullYear() + " " + (today.getHours() < 10 ? '0'+today.getHours() : today.getHours()) + ":" + (today.getMinutes() < 10 ? '0'+today.getMinutes() : today.getMinutes()) + ":" + (today.getSeconds() < 10 ? '0'+today.getSeconds() : today.getSeconds());
			  	return timestamp;
			}
	 		function findLanguage(lang){
    			var findLang = fromLanguages.find( (rec) => rec.value == lang)
    			return findLang.textContent;
   			}
         	function getPdf() {
      	   		window.open('https://' + document.location.hostname + ':' + document.location.port + ctx + '/assets/pdf/K7UsageNotes.pdf')
         	}